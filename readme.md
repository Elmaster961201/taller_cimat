# Como recuperar perfiles usando luz estructurada

En este taller, vamos a dar una introduccion sobre como podemos recuperar
el perfil de una superficie de prueba y obtener una reconstruccion digital
de alla en tres dimensiones. En particular usaremos proyecciones de imagenes
de patrones de franjas como luz estructurada.

# Cosas por hacer

+ Mostrar la figura de la camara en un thread separado.
  Para esto es posible que el enfoque de mostrar el video en la figura
  tenga que cambiar
+ Terminar la clase Projector

# Pasos para correr presentacion

1. Import modules

    from mpl_toolkits.mplot3d import Axes3D
    from skimage.restoration import unwrap_phase
    from Demodulator import Demodulator
    from Projector import Fringes
    from projfringes import ProjectorSystem

2. Create the Proyector System

    psys = ProjectorSystem(1, Fringes(700, 1020, 35))
    psys.start()

3. Show the camera and the fringe pattern

4. Start the phase shifting with the object under test

5. Obtain the image list captured

6. Create the demodulator

7. Compute the frequencies

8. Remove the carrier

9. Show in 3D

    fig = plot.figure()
    ax = fig.gca(projection='3d')
    X,Y = meshgrid(arange(0,phase.shape[1]), arange(0,phase.shape[0]))
    ax.plot_surface(X,Y,phase)