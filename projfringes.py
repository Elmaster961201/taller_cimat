import numpy as np
import cv2
import threading
from Projector import Fringes
from typing import List
import numpy as np
import copy


class ProjectorSystem:
    _list_frames: List[np.ndarray] = ...
    _obj_camera: cv2.VideoCapture = ...
    _flag_close: bool = False
    _obj_th: threading.Thread = ...
    _obj_timer: threading.Timer = ...
    _flag_show_cam: bool = False
    _flag_show_fringes: bool = False
    _flag_show_image: bool = False
    _flag_start_shifting: bool = False
    _obj_lock: threading.Lock = ...
    _image: np.ndarray = ...

    def __init__(self, cam_id:int=0, capture_speed:float=1.0):
        self._idx_cam: int = cam_id
        self._val_capture_speed = capture_speed

    def _init_attributes(self):
        self._obj_fringes = Fringes(800, 1024, 15)
        self._list_frames = []
        self._obj_camera = cv2.VideoCapture(self._idx_cam)
        self._flag_close = False
        self._obj_timer = ...
        self._flag_show_cam = False
        self._flag_show_fringes = False
        self._flag_show_image = False
        self._flag_start_shifting = False
        self._obj_lock: threading.Lock = threading.Lock()
        self._image = None

    def start(self):
        self._init_attributes()
        self._obj_th = threading.Thread(target = self._show, args = ())
        self._obj_timer = threading.Timer(self._val_capture_speed, self._shift)
        self._obj_th.start()

    def _show(self):
        if not self._obj_camera.isOpened():
            self._obj_camera.open(self._idx_cam)
        while True:
            if self._flag_show_cam:
                self._show_camera()
            if self._flag_show_fringes:
                self._show_fringes()
            if self._flag_show_image:
                self._show_image()

            key = cv2.waitKey(16)
            if key & 0xFF == ord('q'):
                if self._flag_show_cam:
                    cv2.destroyWindow('Camera')
                with self._obj_lock:
                    self._flag_show_cam = False
            elif key & 0xFF == ord('s'):
                if self._flag_show_fringes:
                    cv2.destroyWindow('Strips')
                with self._obj_lock:
                    self._flag_show_fringes = False
            elif key & 0xFF == ord('i'):
                if self._flag_show_image:
                    cv2.destroyWindow('Image')
                with self._obj_lock:
                    self._flag_show_image = False

            if self._flag_close:
                cv2.destroyAllWindows()
                break

    def _show_image(self):
        with self._obj_lock:
            if self._image is not None:
                cv2.imshow('Image', self._image)

    def _show_camera(self):
        with self._obj_lock:
            ret, frame = self._obj_camera.read()
            im = cv2.cvtColor(frame, cv2.COLOR_BGRA2GRAY)
            cv2.imshow('Camera', im)

    def _shift(self):
        self.capture_frame()
        if self._flag_start_shifting:
            self._timer = threading.Timer(self._val_capture_speed, self._shift)
            self._timer.start()

    def start_shifting(self, speed = 1.0):
        with self._obj_lock:
            self._list_frames = []
            self._val_capture_speed = speed
            self._flag_start_shifting = True
            self._obj_timer.start()

    def stop_shifting(self):
        with self._obj_lock:
            self._flag_start_shifting = False
            self._obj_timer = threading.Timer(self._val_capture_speed, self._shift)

    def _show_fringes(self):
        with self._obj_lock:
            I = self._obj_fringes.get()
            cv2.imshow('Strips', I)

    def show_fringes(self):
        with self._obj_lock:
            self._flag_show_fringes = True

    def show_camera(self):
        with self._obj_lock:
            self._flag_show_cam = True

    def show_image(self, image: np.ndarray):
        with self._obj_lock:
            image = cv2.normalize(image, None, 255.0, 0.0, cv2.NORM_MINMAX, cv2.CV_8U)
            self._image = image
            self._flag_show_image = True

    def close_camera(self):
        with self._obj_lock:
            self._flag_show_cam = False

    def set_fringes(self, fringes):
        with self._obj_lock:
            self._obj_fringes = fringes

    def capture_frame(self):

        with self._obj_lock:
            if self._obj_camera.isOpened():
                ret, frame = self._obj_camera.read()
                im = cv2.cvtColor(frame, cv2.COLOR_BGRA2GRAY)
                self._obj_fringes.next_strips()
                self._list_frames.append(im)
            else:
                print("Oh oh!!... The camera is not opened")

    def save_images(self):
        n=0
        for im in self._list_frames:
            name = "%.3d.png" % n
            cv2.imwrite(name, im)
            n+= 1

    def get_frames(self)-> List[np.ndarray]:
        with self._obj_lock:
            frames = copy.copy(self._list_frames)
        return frames

    def stop(self):
        with self._obj_lock:
            self._flag_close = True
            self._flag_show_cam = False
            self._flag_show_fringes = False
            self._obj_camera.release()

    def __del__(self):
        self.stop()
        print("Camera closed... Bye!!")