#!/usr/bin/env python
"""
An animated image
"""
from Camera import Camera

cam = Camera()
if cam.open() is False:
    print("Can't open the camera")

cam.show()
cam.join()
