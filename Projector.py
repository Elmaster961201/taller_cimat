import numpy as np
import cv2
import threading


def get_strips(N, M, k, orientation, step):
    x,y = np.ogrid[0:N, 0:M]

    if orientation == 'horizontal':
        I = np.cos(2.0*np.pi*k*x / M + y*0 + step)
    else:
        I = np.cos(2.0 * np.pi * k * y / N + step + x*0)

    return I


class Projector:
    def __init__(self, N = 512, M = 512, k = 30, orientation = 'vertical', alpha = np.pi/2.0):
        self.N = N                          # Vertical Size
        self.M = M                          # Horizontal Size
        self.k = k                          # Number of Stripes
        self.orientation = orientation      # Stripes Orientation
        self.alpha = alpha                  # Step Size
        self.n = 0                          # Step Value

    def start(self):        
        self.I = get_strips(self.N, self.M, self.k, self.orientation, self.alpha*self.n)
        th = threading.Thread(target = self.show_strips, args = ())
        #th.daemon = True
        th.start()

    def next_strips(self):
        self.n = self.n + 1
        self.I = get_strips(self.N, self.M, self.k, self.orientation, self.alpha * self.n)

    def prev_strips(self):
        self.n = self.n - 1
        self.I = get_strips(self.N, self.M, self.k, self.orientation, self.alpha * self.n)

    def show_strips(self):
        while 1:
            I = (self.I+1.0)/2.0
            cv2.imshow('Strips', I)
            if cv2.waitKey(32) & 0xFF == ord('p'):
                break
        cv2.destroyAllWindows()


class Fringes(Projector):
    def __init__(self, N=512, M=512, k=30, orientation='vertical', alpha=np.pi / 2.0):
        super().__init__(N, M, k, orientation, alpha)
        self.start()

    def start(self):
        self.I = get_strips(self.N, self.M, self.k, self.orientation, self.alpha * self.n)

    def show_strips(self):
        pass

    def get(self):
        threshold = 0.7

        im = (self.I + 1.0) / 2.0

        im[im < threshold] = 0.0
        return im

