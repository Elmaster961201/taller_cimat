from Demodulator import *
import cv2
from skimage.restoration import unwrap_phase
from mayavi.mlab import *
import numpy as np

demo = Demodulator()

L = []
L.append(cv2.imread('-2.bmp', 0))
L.append(cv2.imread('-1.bmp', 0))
L.append(cv2.imread('0.bmp', 0))
L.append(cv2.imread('1.bmp', 0))
L.append(cv2.imread('2.bmp', 0))

demo.set_list(L)

phase_carrier = demo.get_phase()

demo.remove()

phase = demo.get_phase()

phase = unwrap_phase(phase)

phase = cv2.GaussianBlur(phase,(41,41),0)

mag = demo.get_magnitude()

phase_carrier = cv2.normalize(phase_carrier,None,1,0,cv2.NORM_MINMAX)
cv2.imshow('Phase with Carrier', phase_carrier)

phase = cv2.normalize(phase,None,1,0,cv2.NORM_MINMAX)
cv2.imshow('Phase', phase)

mag = cv2.normalize(mag, None,1,0,cv2.NORM_MINMAX)
cv2.imshow('Magnitude', mag)

x, y = np.mgrid[0:256:1, 0:256:1]
s = surf(x, y, phase*100)

cv2.waitKey(0)

cv2.destroyAllWindows()