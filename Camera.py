import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import cv2
import threading


def show_frame(frame):
    cv2.imshow('Captured Frame', frame)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

class Camera(object):    
    def __init__(self, ncam=0, N = 512, M = 512, k = 30, orientation = 'vertical', alpha = np.pi/2.0):
        self.frame_list = []
        self.capturer = cv2.VideoCapture(ncam)
        self.ncam = ncam

        self._close = False

    def capture_frame(self):

        if self.capturer.isOpened():
            ret, frame = self.capturer.read()

            im = cv2.cvtColor(frame, cv2.COLOR_BGRA2GRAY)

            self.frame_list.append(im)
        else:
            print("Oh oh!!... The camera is not opened")

    def get_frame_list(self):
        return self.frame_list

    def show(self):
        try:
            self._close = False
            threading.Thread(target=self._show_camera).start()
        except:
            print("Error: Unable to start thread")

    def _show_camera(self):
        if not self.capturer.isOpened():
            cv2.namedWindow('Camera')
            self.capturer.open(self.ncam)
        while not self._close:
            ret, frame = self.capturer.read()

            im = cv2.cvtColor(frame, cv2.COLOR_BGRA2GRAY)

            cv2.imshow('Camera', im)

            if cv2.waitKey(32) & 0xFF == ord('q'):
                break

        self.capturer.release()
        cv2.destroyWindow('Camera')

    def close(self):
        if self.capturer.isOpened():
            self._close=True

    def open(self):
        return self.capturer.isOpened()

    def __del__(self):
        self.close()
        print("Camera closed... Bye!!")
